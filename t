/* CSS for title*/
.Title-color-1{
        color: $navbar-color;
    }
     .Title-color-2{
        color: $primary-color;
    }
     .logo-text{
         padding-left: 0px;
         a{
         font-size: 30px;
             text-decoration: none;
         }
     }
.navbar-brand {
         padding: 15px 0px;
     }
/* CSS for title finish*/

/* padding for header*/
 #page-header {
     padding-top: 20px;
    padding-left: 50px;
     padding-right: 50px;
    
     
/* navbar color setting*/
.navbar-default {
    background-color:white;
    border-color: white;
}
      

     
/*     .navbar-font{
         font-family: "Haettenschweiler";
         padding-left: 100px;
          padding-right: 0px;
     }*/
     
    /* .nav-icon{
         //padding-top: 10px;
         color: $navbar-color;
         i{
             padding-top: 14px;
             padding-right: 0px;
         }
     }*/
    
     
     @media (max-width: 990px) {
    .navbar-header {
        float: none;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-collapse.collapse {
        display: none!important;
    }
    .navbar-nav {
        float: none!important;
        margin: 7.5px -15px;
    }
    .navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
}
     
     
     
     /*/main nav bar otions CSS*/
     .main-nav{
         padding-left: 70px;
     }
     .main-nav>li>a {
    color:black;
    font-size: 20px;
         font-family: "Haettenschweiler";
         //margin-left: 9px;
}
     
    /* @media (max-width:1120px){
         
         .navbar-nav>li>a {
             font-size: 17px;
         }
     }*/
     
         
               
    
.navbar-default .navbar-collapse, .navbar-default .navbar-form {
        text-align: center;
}
     
    /* DropDown(world) CSS */
      li.dropdown:hover ul.dropdown-menu {
            display: block;
        }
    .dropdown-menu>li>a {
        font-size: 20px;
        clear: both;
        font-weight: 400;
        line-height: 1.42857143;
        color: black;
        font-family: "Haettenschweiler";
       
        //font-size: 13px;
        white-space: nowrap;
        margin: 0px;
        
    }
    .dropdown-menu {
        -webkit-box-shadow: none;
        left: -11px;
       
        border: none;
         border-bottom: thick solid #bdc3c7;
       
        text-align: center;
        //min-width: 130px;
        top: 80%;
    }
    .dropdown-menu {
        li {
            a:hover {
                background: none;
                    }
        }
    }
     /* DropDown(world) CSS  ENDS*/
}

/* nav bar testing css */

@media (max-width:991px) { 
	.custom-navbar .navbar-right {
	   // float: right;
	    padding-right: 15px;
	}
	.custom-navbar .nav.navbar-nav.navbar-right li {
	    //float: right;
	}
  	.custom-navbar .nav.navbar-nav.navbar-right li > a {
	    padding:8px 5px;
	}
	.custom-navbar .navbar-toggle {
	    /*float: left*/
	}
	.custom-navbar .navbar-header {
	    //float: left;
	    width: auto!important;
	}
	.custom-navbar .navbar-collapse {
	    clear: both;
	    float: none;
	}
}
/* nav bar testing css  finish*/
/*search bar CSS*/



.search-icon{
    padding-top: 15px;
    
    
    
    input{
-webkit-border-radius: 50px;
-moz-border-radius: 50px;
border-radius: 50px;
}
    }

.user-icon{
     padding-top: 15px;
}

.search {
    width: 150px;
    max-width:0;
    padding: 5px;
    transition: all .5s ease;
    position:absolute;
    right:20px;
    //box-sizing:border-box;
    opacity:0;
    
}
.search.expanded {
    max-width:150px;
    opacity:0.7;
    height: 33px;
    margin-right: 5px;
    top: 70%;
  
}
.icon {
    width: 20px;
    height: 15px;
    //background: red;
    position: absolute;
    right: 0;
}

.user-icon{
    
    float: right;
    
    
}
/*testing search bar finish*/


/*searchbar css finish*/

.main-container{
   /* padding-left: 25px;
        padding-right:25px;*/
    .navbar-collapse{
        padding-top: 15px;
        padding-left: 0px;
    padding-right: 0px;
    }
        
}
